<?php

namespace App\Services;

use Symfony\Contracts\HttpClient\HttpClientInterface;

class ApiCallService
{
    private $client;
    private $api;
    private $star;


    public function __construct(HttpClientInterface $client)
    {
        $this->client = $client;
        $this->star = 'https://data.explore.star.fr/api/v2/catalog/datasets/';
        $this->api = $this->star . 'tco-bus-vehicules-geoposition-suivi-new-billetique-tr/';
    }

    public function getPositions(): array
    {
        $response = $this->client->request(
            'GET',
            $this->api . 'records?limit=100&offset=0'
        );
        $response = $response->toArray()['records'];

        for ($i = 0; $i < count($response); $i++) {
            $coordonnees = $response[$i]['record']['fields']['coordonnees'];
            $nomcourtligne = $response[$i]['record']['fields']['nomcourtligne'];
            $destination = $response[$i]['record']['fields']['destination'];
            $datas[] = [
                'nomcourtligne' => $nomcourtligne,
                'destination' => $destination,
                'coordonnees' => $coordonnees,
            ];
        }

        return $datas;
    }

    public function getPositionsByBus($nombus): array
    {
        $response = $this->client->request(
            'GET',
            $this->api . 'records?refine=nomcourtligne%3A' . $nombus . '%20&timezone=UTC'
        );
        $response = $response->toArray()['records'];

        for ($i = 0; $i < count($response); $i++) {
            $coordonnees = $response[$i]['record']['fields']['coordonnees'];
            $nomcourtligne = $response[$i]['record']['fields']['nomcourtligne'];
            $destination = $response[$i]['record']['fields']['destination'];
            $datas[] = [
                'nomcourtligne' => $nomcourtligne,
                'destination' => $destination,
                'coordonnees' => $coordonnees,
            ];
        }

        return $datas;
    }

    public function getNomCourtBus(): array
    {
        $response = $this->client->request(
            'GET',
            $this->api . 'records?limit=100&offset=0'
        );
        $response = $response->toArray()['records'];

        for ($i = 0; $i < count($response); $i++) {
            $nomcourtligne[$response[$i]['record']['fields']['nomcourtligne']]
            = $response[$i]['record']['fields']['nomcourtligne'];
        }

        return array_unique($nomcourtligne);
    }
}
