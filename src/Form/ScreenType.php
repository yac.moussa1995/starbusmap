<?php

namespace App\Form;

use App\Entity\Screen;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use App\Services\ApiCallService;

class ScreenType extends AbstractType
{
    private $apiCallService;

    public function __construct(ApiCallService $apiCallService)
    {
        $this->apiCallService = $apiCallService;
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('title', null, [
                'label' => 'Libelé'])
        ;
        $builder->add('nomCourtLigne', ChoiceType::class, [
            'choices'  => $this->apiCallService->getNomCourtBus(),
            'label' => 'Nom de votre ligne'
        ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Screen::class,
        ]);
    }
}
