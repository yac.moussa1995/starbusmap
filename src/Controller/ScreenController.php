<?php

namespace App\Controller;

use App\Entity\Screen;
use App\Form\ScreenType;
use App\Repository\ScreenRepository;
use App\Entity\ScreenBus;
use App\Repository\ScreenBusRepository;
use Doctrine\Persistence\ManagerRegistry;
use App\Services\ApiCallService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/screen')]
class ScreenController extends AbstractController
{
    #[Route('/', name: 'app_screen_index', methods: ['GET'])]
    public function index(ScreenRepository $screenRepository): Response
    {
        $user = $this->getUser();
        return $this->render('screen/index.html.twig', [
            'screens' => $screenRepository->findBy(['screenUser' => $user->getId()])
        ]);
    }

    #[Route('/new', name: 'app_screen_new', methods: ['GET', 'POST'])]
    public function new(
        Request $request,
        ScreenRepository $screenRepository,
        ManagerRegistry $doctrine,
        ApiCallService $apiCallService
    ): Response {
        $entityManager = $doctrine->getManager();
        $listBus = $apiCallService->getNomCourtBus();
        $user = $this->getUser();
        $screen = new Screen();
        $form = $this->createForm(ScreenType::class, $screen);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $screen->setScreenUser($user);
            $screen->setNomCourtLigne($form
                                 ->get('nomCourtLigne')
                                 ->getData());
            $screenRepository->add($screen, true);

            $allBusData = $apiCallService
                            ->getPositionsByBus($form
                            ->get('nomCourtLigne')->getData());

            for ($i = 0; $i < count($allBusData); $i++) {
                $screenBus = new ScreenBus();
                $screenBus->setDestination($allBusData[$i]['destination']);
                $screenBus->setLat($allBusData[$i]['coordonnees']['lat']);
                $screenBus->setLon($allBusData[$i]['coordonnees']['lon']);
                $screenBus->setScreen($screen);
                $entityManager->persist($screenBus);
                $entityManager->flush();
            }

            return $this->redirectToRoute('app_screen_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('screen/new.html.twig', [
            'screen' => $screen,
            'form' => $form,
            'listbus' => $listBus,
        ]);
    }

    #[Route('/{id}', name: 'app_screen_show', methods: ['GET'])]
    public function show(ScreenBusRepository $screenBusRepository, Screen $screen): Response
    {
        $busdata = $screenBusRepository->findBy(['screen' => $screen]);
        ;

        for ($i = 0; $i < count($busdata); $i++) {
            $coordonnees['lat'] = $busdata[$i]->getLat();
            $coordonnees['lon'] = $busdata[$i]->getLon();
            $destination = $busdata[$i]->getDestination();
            $datas[] = [
                'destination' => $destination,
                'coordonnees' => $coordonnees,
                'nomcourtligne' => $screen->getNomCourtLigne(),
            ];
        }

        return $this->render('screen/show.html.twig', [
            'screen' => $screen,
            'datas' => $datas,
        ]);
    }
}
