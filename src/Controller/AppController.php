<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Services\ApiCallService;
use Symfony\Component\HttpFoundation\RedirectResponse;

class AppController extends AbstractController
{
    #[Route('/app/{bus}', name: 'app_index', defaults:['bus' => 'all'])]
    public function index(string $bus, ApiCallService $apiCallService): Response
    {
        if ($bus == 'all') {
            $datas = $apiCallService->getPositions();
            return $this->render('app/index.html.twig', [
                'datas' => $datas,
            ]);
        } else {
            $datas = $apiCallService->getPositionsByBus($bus);
            return $this->render('app/index.html.twig', [
                'datas' => $datas,
            ]);
        }
    }

    #[Route('/', name: 'app')]
    public function app(): RedirectResponse
    {
        return $this->redirectToRoute('app_index');
    }
}
