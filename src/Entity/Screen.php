<?php

namespace App\Entity;

use App\Repository\ScreenRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ScreenRepository::class)]
class Screen
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    private $title;

    #[ORM\Column(type: 'datetime')]
    private $screenDate;

    #[ORM\ManyToOne(targetEntity: User::class, inversedBy: 'screens')]
    #[ORM\JoinColumn(nullable: false)]
    private $screenUser;

    #[ORM\OneToMany(mappedBy: 'screen', targetEntity: ScreenBus::class, orphanRemoval: true)]
    private $screenBuses;

    #[ORM\Column(type: 'string', length: 255)]
    private $nomCourtLigne;

    public function __construct()
    {
        $this->screenBuses = new ArrayCollection();
        $this->screenDate = new \DateTimeImmutable();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getScreenDate(): ?\DateTimeInterface
    {
        return $this->screenDate;
    }

    public function setScreenDate(\DateTimeInterface $screenDate): self
    {
        $this->screenDate = $screenDate;

        return $this;
    }

    public function getScreenUser(): ?User
    {
        return $this->screenUser;
    }

    public function setScreenUser(?User $screenUser): self
    {
        $this->screenUser = $screenUser;

        return $this;
    }

    /**
     * @return Collection<int, ScreenBus>
     */
    public function getScreenBuses(): Collection
    {
        return $this->screenBuses;
    }

    public function addScreenBus(ScreenBus $screenBus): self
    {
        if (!$this->screenBuses->contains($screenBus)) {
            $this->screenBuses[] = $screenBus;
            $screenBus->setScreen($this);
        }

        return $this;
    }

    public function removeScreenBus(ScreenBus $screenBus): self
    {
        if ($this->screenBuses->removeElement($screenBus)) {
            // set the owning side to null (unless already changed)
            if ($screenBus->getScreen() === $this) {
                $screenBus->setScreen(null);
            }
        }

        return $this;
    }

    public function getNomCourtLigne(): ?string
    {
        return $this->nomCourtLigne;
    }

    public function setNomCourtLigne(string $nomCourtLigne): self
    {
        $this->nomCourtLigne = $nomCourtLigne;

        return $this;
    }
}
