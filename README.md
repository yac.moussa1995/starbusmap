
# Star Bus Map

Application to see Star buses in real time on an Openstreet map


## Prerequisites

**PHP 8.1 extensions**

Ctype, iconv, PCRE, Session, PDO_PGSQL, SimpleXML, and Tokenizer;

**Docker**

docker-compose

**Nodejs 14+**

yarn 


## Run Locally

Clone the project

```bash
  git clone https://gitlab.com/yac.moussa1995/starbusmap.git
```

Go to the project directory

```bash
  cd starbusmap
```

Install dependencies

```bash
  composer install
  yarn install
```
Set-up Database and install migrations

```bash
  sudo docker-compose up -d
  sudo php bin/console make:migration
  sudo php bin/console doctrine:migrations:migrate

```

Start the server

```bash
  yarn encore dev
  sudo symfony serve -d
```



## License

[MIT](https://choosealicense.com/licenses/mit/)

